package ee.itcollege.i377.entities;

import java.io.Serializable;
<<<<<<< HEAD
import java.util.Date;
=======
>>>>>>> ttalviste
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
<<<<<<< HEAD
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
=======

>>>>>>> ttalviste
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

<<<<<<< HEAD

=======
>>>>>>> ttalviste
/**
 * The persistent class for the RIIK database table.
 * 
 */
@Entity
@RooJavaBean
@RooToString
@RooEntity
<<<<<<< HEAD
public class Riik implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="RIIK_ID")
	private Long riikId;

	@Column(name="ANSI_KOOD")
	private String ansiKood;

	private String avaja;

    @Temporal( TemporalType.DATE)
    @DateTimeFormat(style="M-")
	private Date avatud;

	@Column(name="ISO_KOOD")
	private String isoKood;

	private String kommentaar;

    @Temporal( TemporalType.DATE)
    @DateTimeFormat(style="M-")
	private Date muudetud;

	private String muutja;

    @Temporal( TemporalType.DATE)
    @DateTimeFormat(style="M-")
	private Date suletud;

	private String sulgeja;

	//bi-directional many-to-one association to Kodakondsus
	@OneToMany(mappedBy="riik")
	private Set<Kodakondsus> kodakondsuses;

    public Riik() {
    }
=======
public class Riik extends HistoryHandlerEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "RIIK_ID")
	private Long riikId;

	@Column(name = "ANSI_KOOD")
	private String ansiKood;

	@Column(name = "ISO_KOOD")
	private String isoKood;

	// bi-directional many-to-one association to Kodakondsus
	@OneToMany(mappedBy = "riik")
	private Set<Kodakondsus> kodakondsuses;

	public Riik() {
	}
>>>>>>> ttalviste

	public Long getRiikId() {
		return this.riikId;
	}

	public void setRiikId(Long riikId) {
		this.riikId = riikId;
	}

	public String getAnsiKood() {
		return this.ansiKood;
	}

	public void setAnsiKood(String ansiKood) {
		this.ansiKood = ansiKood;
	}

<<<<<<< HEAD
	public String getAvaja() {
		return this.avaja;
	}

	public void setAvaja(String avaja) {
		this.avaja = avaja;
	}

	public Date getAvatud() {
		return this.avatud;
	}

	public void setAvatud(Date avatud) {
		this.avatud = avatud;
	}

=======
>>>>>>> ttalviste
	public String getIsoKood() {
		return this.isoKood;
	}

	public void setIsoKood(String isoKood) {
		this.isoKood = isoKood;
	}

<<<<<<< HEAD
	public String getKommentaar() {
		return this.kommentaar;
	}

	public void setKommentaar(String kommentaar) {
		this.kommentaar = kommentaar;
	}

	public Date getMuudetud() {
		return this.muudetud;
	}

	public void setMuudetud(Date muudetud) {
		this.muudetud = muudetud;
	}

	public String getMuutja() {
		return this.muutja;
	}

	public void setMuutja(String muutja) {
		this.muutja = muutja;
	}

	public Date getSuletud() {
		return this.suletud;
	}

	public void setSuletud(Date suletud) {
		this.suletud = suletud;
	}

	public String getSulgeja() {
		return this.sulgeja;
	}

	public void setSulgeja(String sulgeja) {
		this.sulgeja = sulgeja;
	}

=======
>>>>>>> ttalviste
	public Set<Kodakondsus> getKodakondsuses() {
		return this.kodakondsuses;
	}

	public void setKodakondsuses(Set<Kodakondsus> kodakondsuses) {
		this.kodakondsuses = kodakondsuses;
	}
<<<<<<< HEAD
	
=======

>>>>>>> ttalviste
}