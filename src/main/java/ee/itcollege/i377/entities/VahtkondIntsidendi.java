package ee.itcollege.i377.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

<<<<<<< HEAD

=======
>>>>>>> ttalviste
/**
 * The persistent class for the VAHTKOND_INTSIDENDIS database table.
 * 
 */
@Entity
@RooJavaBean
@RooToString
@RooEntity
<<<<<<< HEAD
@Table(name="VAHTKOND_INTSIDENDIS")
public class VahtkondIntsidendi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="VAHTKOND_INTSIDENDIS_ID")
	private Long vahtkondIntsidendisId;

    @Temporal( TemporalType.DATE)
    @DateTimeFormat(style="M-")
	private Date alates;

	private String avaja;

    @Temporal( TemporalType.DATE)
    @DateTimeFormat(style="M-")
	private Date avatud;

	private String kirjeldus;

	private String kommentaar;

    @Temporal( TemporalType.DATE)
    @DateTimeFormat(style="M-")
	private Date kuni;

    @Temporal( TemporalType.DATE)
    @DateTimeFormat(style="F-")
	private Date muudetud;

	private String muutja;

    @Temporal( TemporalType.DATE)
    @DateTimeFormat(style="M-")
	private Date suletud;

	private String sulgeja;

	//bi-directional many-to-one association to PiirivalvurIntsidendi
	@OneToMany(mappedBy="vahtkondIntsidendi")
	private Set<PiirivalvurIntsidendi> piirivalvurIntsidendis;

	//bi-directional many-to-one association to Intsident
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="INTSIDENT_ID")
	private Intsident intsident;

	//bi-directional many-to-one association to Vahtkond
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VAHTKOND_ID")
	private Vahtkond vahtkond;

    public VahtkondIntsidendi() {
    }
=======
@Table(name = "VAHTKOND_INTSIDENDIS")
public class VahtkondIntsidendi extends HistoryHandlerEntity implements
		Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "VAHTKOND_INTSIDENDIS_ID")
	private Long vahtkondIntsidendisId;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(style = "M-")
	private Date alates;

	private String kirjeldus;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(style = "M-")
	private Date kuni;

	// bi-directional many-to-one association to PiirivalvurIntsidendi
	@OneToMany(mappedBy = "vahtkondIntsidendi")
	private Set<PiirivalvurIntsidendi> piirivalvurIntsidendis;

	// bi-directional many-to-one association to Intsident
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "INTSIDENT_ID")
	private Intsident intsident;

	// bi-directional many-to-one association to Vahtkond
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "VAHTKOND_ID")
	private Vahtkond vahtkond;

	public VahtkondIntsidendi() {
	}
>>>>>>> ttalviste

	public Long getVahtkondIntsidendisId() {
		return this.vahtkondIntsidendisId;
	}

	public void setVahtkondIntsidendisId(Long vahtkondIntsidendisId) {
		this.vahtkondIntsidendisId = vahtkondIntsidendisId;
	}

	public Date getAlates() {
		return this.alates;
	}

	public void setAlates(Date alates) {
		this.alates = alates;
	}

<<<<<<< HEAD
	public String getAvaja() {
		return this.avaja;
	}

	public void setAvaja(String avaja) {
		this.avaja = avaja;
	}

	public Date getAvatud() {
		return this.avatud;
	}

	public void setAvatud(Date avatud) {
		this.avatud = avatud;
	}

	public String getKirjeldus() {
		return this.kirjeldus;
	}

	public void setKirjeldus(String kirjeldus) {
		this.kirjeldus = kirjeldus;
	}

	public String getKommentaar() {
		return this.kommentaar;
	}

	public void setKommentaar(String kommentaar) {
		this.kommentaar = kommentaar;
	}

=======
>>>>>>> ttalviste
	public Date getKuni() {
		return this.kuni;
	}

	public void setKuni(Date kuni) {
		this.kuni = kuni;
	}

<<<<<<< HEAD
	public Date getMuudetud() {
		return this.muudetud;
	}

	public void setMuudetud(Date muudetud) {
		this.muudetud = muudetud;
	}

	public String getMuutja() {
		return this.muutja;
	}

	public void setMuutja(String muutja) {
		this.muutja = muutja;
	}

	public Date getSuletud() {
		return this.suletud;
	}

	public void setSuletud(Date suletud) {
		this.suletud = suletud;
	}

	public String getSulgeja() {
		return this.sulgeja;
	}

	public void setSulgeja(String sulgeja) {
		this.sulgeja = sulgeja;
	}

=======
>>>>>>> ttalviste
	public Set<PiirivalvurIntsidendi> getPiirivalvurIntsidendis() {
		return this.piirivalvurIntsidendis;
	}

<<<<<<< HEAD
	public void setPiirivalvurIntsidendis(Set<PiirivalvurIntsidendi> piirivalvurIntsidendis) {
		this.piirivalvurIntsidendis = piirivalvurIntsidendis;
	}
	
=======
	public void setPiirivalvurIntsidendis(
			Set<PiirivalvurIntsidendi> piirivalvurIntsidendis) {
		this.piirivalvurIntsidendis = piirivalvurIntsidendis;
	}

>>>>>>> ttalviste
	public Intsident getIntsident() {
		return this.intsident;
	}

	public void setIntsident(Intsident intsident) {
		this.intsident = intsident;
	}
<<<<<<< HEAD
	
=======

>>>>>>> ttalviste
	public Vahtkond getVahtkond() {
		return this.vahtkond;
	}

	public void setVahtkond(Vahtkond vahtkond) {
		this.vahtkond = vahtkond;
	}
<<<<<<< HEAD
	
=======

>>>>>>> ttalviste
}