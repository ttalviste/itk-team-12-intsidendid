package ee.itcollege.i377.entities;

import java.io.Serializable;
<<<<<<< HEAD
import java.util.Date;
=======
>>>>>>> ttalviste
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
<<<<<<< HEAD
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
=======

>>>>>>> ttalviste
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

<<<<<<< HEAD

=======
>>>>>>> ttalviste
/**
 * The persistent class for the OBJEKT database table.
 * 
 */
@Entity
@RooJavaBean
@RooToString
@RooEntity
<<<<<<< HEAD
public class Objekt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="OBJEKT_ID")
	private Long objektId;

	private String avaja;

    @Temporal( TemporalType.DATE)
    @DateTimeFormat(style="M-")
	private Date avatud;

	private String kommentaar;

    @Temporal( TemporalType.DATE)
    @DateTimeFormat(style="M-")
	private Date muudetud;

	private String muutja;

	private String nimetus;

	@Column(name="OBJEKT_LIIK_ID")
	private int objektLiikId;

    @Temporal( TemporalType.DATE)
    @DateTimeFormat(style="M-")
	private Date suletud;

	private String sulgeja;

	//bi-directional many-to-one association to ObjektIntsidendi
	@OneToMany(mappedBy="objekt")
	private Set<ObjektIntsidendi> objektIntsidendis;

	//bi-directional many-to-one association to Piiririkkuja
	@OneToMany(mappedBy="objekt")
	private Set<Piiririkkuja> piiririkkujas;

    public Objekt() {
    }
=======
public class Objekt extends HistoryHandlerEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "OBJEKT_ID")
	private Long objektId;

	private String nimetus;

	@Column(name = "OBJEKT_LIIK_ID")
	private int objektLiikId;

	// bi-directional many-to-one association to ObjektIntsidendi
	@OneToMany(mappedBy = "objekt")
	private Set<ObjektIntsidendi> objektIntsidendis;

	// bi-directional many-to-one association to Piiririkkuja
	@OneToMany(mappedBy = "objekt")
	private Set<Piiririkkuja> piiririkkujas;

	public Objekt() {
	}
>>>>>>> ttalviste

	public Long getObjektId() {
		return this.objektId;
	}

	public void setObjektId(Long objektId) {
		this.objektId = objektId;
	}

<<<<<<< HEAD
	public String getAvaja() {
		return this.avaja;
	}

	public void setAvaja(String avaja) {
		this.avaja = avaja;
	}

	public Date getAvatud() {
		return this.avatud;
	}

	public void setAvatud(Date avatud) {
		this.avatud = avatud;
	}

	public String getKommentaar() {
		return this.kommentaar;
	}

	public void setKommentaar(String kommentaar) {
		this.kommentaar = kommentaar;
	}

	public Date getMuudetud() {
		return this.muudetud;
	}

	public void setMuudetud(Date muudetud) {
		this.muudetud = muudetud;
	}

	public String getMuutja() {
		return this.muutja;
	}

	public void setMuutja(String muutja) {
		this.muutja = muutja;
	}

=======
>>>>>>> ttalviste
	public String getNimetus() {
		return this.nimetus;
	}

	public void setNimetus(String nimetus) {
		this.nimetus = nimetus;
	}

	public int getObjektLiikId() {
		return this.objektLiikId;
	}

	public void setObjektLiikId(int objektLiikId) {
		this.objektLiikId = objektLiikId;
	}

<<<<<<< HEAD
	public Date getSuletud() {
		return this.suletud;
	}

	public void setSuletud(Date suletud) {
		this.suletud = suletud;
	}

	public String getSulgeja() {
		return this.sulgeja;
	}

	public void setSulgeja(String sulgeja) {
		this.sulgeja = sulgeja;
	}

=======
>>>>>>> ttalviste
	public Set<ObjektIntsidendi> getObjektIntsidendis() {
		return this.objektIntsidendis;
	}

	public void setObjektIntsidendis(Set<ObjektIntsidendi> objektIntsidendis) {
		this.objektIntsidendis = objektIntsidendis;
	}
<<<<<<< HEAD
	
=======

>>>>>>> ttalviste
	public Set<Piiririkkuja> getPiiririkkujas() {
		return this.piiririkkujas;
	}

	public void setPiiririkkujas(Set<Piiririkkuja> piiririkkujas) {
		this.piiririkkujas = piiririkkujas;
	}
<<<<<<< HEAD
	
=======

>>>>>>> ttalviste
}