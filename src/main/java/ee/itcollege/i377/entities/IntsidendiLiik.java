package ee.itcollege.i377.entities;

import java.io.Serializable;
<<<<<<< HEAD
import java.util.Date;
=======
>>>>>>> ttalviste
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
<<<<<<< HEAD
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
=======

>>>>>>> ttalviste
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

<<<<<<< HEAD

=======
>>>>>>> ttalviste
/**
 * The persistent class for the INTSIDENDI_LIIK database table.
 * 
 */
@Entity
@RooJavaBean
@RooToString
@RooEntity
<<<<<<< HEAD
@Table(name="INTSIDENDI_LIIK")
public class IntsidendiLiik implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="INTSIDENDI_LIIK_ID")
	private Long intsidendiLiikId;

	private String avaja;

    @Temporal( TemporalType.DATE)
    @DateTimeFormat(style="M-")
	private Date avatud;

	private String kommentaar;

	private String kood;

    @Temporal( TemporalType.DATE)
    @DateTimeFormat(style="M-")
	private Date muudetud;

	private String muutja;

	private String nimetus;

    @Temporal( TemporalType.DATE)
    @DateTimeFormat(style="M-")
	private Date suletud;

	private String sulgeja;

	//bi-directional many-to-one association to Intsident
	@OneToMany(mappedBy="intsidendiLiik")
	private Set<Intsident> intsidents;

    public IntsidendiLiik() {
    }
=======
@Table(name = "INTSIDENDI_LIIK")
public class IntsidendiLiik extends HistoryHandlerEntity implements
		Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "INTSIDENDI_LIIK_ID")
	private Long intsidendiLiikId;

	private String kood;

	private String nimetus;

	// bi-directional many-to-one association to Intsident
	@OneToMany(mappedBy = "intsidendiLiik")
	private Set<Intsident> intsidents;

	public IntsidendiLiik() {
	}
>>>>>>> ttalviste

	public Long getIntsidendiLiikId() {
		return this.intsidendiLiikId;
	}

	public void setIntsidendiLiikId(Long intsidendiLiikId) {
		this.intsidendiLiikId = intsidendiLiikId;
	}

<<<<<<< HEAD
	public String getAvaja() {
		return this.avaja;
	}

	public void setAvaja(String avaja) {
		this.avaja = avaja;
	}

	public Date getAvatud() {
		return this.avatud;
	}

	public void setAvatud(Date avatud) {
		this.avatud = avatud;
	}

	public String getKommentaar() {
		return this.kommentaar;
	}

	public void setKommentaar(String kommentaar) {
		this.kommentaar = kommentaar;
	}

=======
>>>>>>> ttalviste
	public String getKood() {
		return this.kood;
	}

	public void setKood(String kood) {
		this.kood = kood;
	}

<<<<<<< HEAD
	public Date getMuudetud() {
		return this.muudetud;
	}

	public void setMuudetud(Date muudetud) {
		this.muudetud = muudetud;
	}

	public String getMuutja() {
		return this.muutja;
	}

	public void setMuutja(String muutja) {
		this.muutja = muutja;
	}

=======
>>>>>>> ttalviste
	public String getNimetus() {
		return this.nimetus;
	}

	public void setNimetus(String nimetus) {
		this.nimetus = nimetus;
	}

<<<<<<< HEAD
	public Date getSuletud() {
		return this.suletud;
	}

	public void setSuletud(Date suletud) {
		this.suletud = suletud;
	}

	public String getSulgeja() {
		return this.sulgeja;
	}

	public void setSulgeja(String sulgeja) {
		this.sulgeja = sulgeja;
	}

=======
>>>>>>> ttalviste
	public Set<Intsident> getIntsidents() {
		return this.intsidents;
	}

	public void setIntsidents(Set<Intsident> intsidents) {
		this.intsidents = intsidents;
	}
<<<<<<< HEAD
	
=======

>>>>>>> ttalviste
}